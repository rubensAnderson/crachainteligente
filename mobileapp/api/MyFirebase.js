import {ToastAndroid} from 'react-native';

import firebase from "firebase";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyCKwsKKLVFvBNR2Vy29YyIJBcPYQVxVdVE",
  authDomain: "crachainteligence.firebaseapp.com",
  databaseURL: "https://crachainteligence.firebaseio.com/",
  storageBucket: "crachainteligence.appspot.com"
};

firebase.initializeApp(config);

class MyFirebase {

  userLogin = (email, password) => {
    return new Promise(resolve => {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .catch(error => {
          switch (error.code) {
            case 'auth/invalid-email':
              {/*console.warn('Invalid email address format.');*/}
              ToastAndroid.showWithGravity(
                'Invalid email address format.',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
              break;
            case 'auth/user-not-found':
              ToastAndroid.showWithGravity(
                'User not found.',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
              break;
            case 'auth/wrong-password':
              {/*console.warn('Invalid email address or password');*/}
              ToastAndroid.showWithGravity(
                'Invalid email address or password',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
              break;
            default:
              {/*console.warn('Check your internet connection');*/}
              ToastAndroid.showWithGravity(
                'Check your internet connection',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
          }
          resolve(null);
        }).then(user => {
        if (user) {
          console.log(user)
          resolve(user);
        }
      });
    })
  };

  createFirebaseAccount = (name, email, password) => {
    return new Promise(resolve => {
      firebase.auth().createUserWithEmailAndPassword(email, password).catch(error => {
        switch (error.code) {
          case 'auth/email-already-in-use':
            console.log('This email address is already taken');
            ToastAndroid.showWithGravity(
              'This email address is already taken',
              ToastAndroid.SHORT,
              ToastAndroid.CENTER,
            );
            break;
          case 'auth/invalid-email':
            console.log('Invalid e-mail address format');
            ToastAndroid.showWithGravity(
              'Invalid e-mail address format',
              ToastAndroid.SHORT,
              ToastAndroid.CENTER,
            );
            break;
          case 'auth/weak-password':
            console.log('Password is too weak');
            ToastAndroid.showWithGravity(
              'Password is too weak',
              ToastAndroid.SHORT,
              ToastAndroid.CENTER,
            );
            break;
          default:
            console.log('Check your internet connection');
            ToastAndroid.showWithGravity(
              'Check your internet connection',
              ToastAndroid.SHORT,
              ToastAndroid.CENTER,
            );
        }
        resolve(false);
      }).then(info => {
        if (info) {
          firebase.auth().currentUser.updateProfile({
            displayName: name
          });
          resolve(true);
        }
      });
    });
  };

  sendEmailWithPassword = (email) => {
    return new Promise(resolve => {
      firebase.auth().sendPasswordResetEmail(email)
        .then(() => {
          {/*console.warn('Email with new password has been sent');*/}
          ToastAndroid.showWithGravity(
                'O máximo de jogadores por partida é 7 ',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
          resolve(true);
        }).catch(error => {
          switch (error.code) {
            case 'auth/invalid-email':
              {/*console.warn('Invalid email address format');*/}
              ToastAndroid.showWithGravity(
                'O máximo de jogadores por partida é 7 ',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
              break;
            case 'auth/user-not-found':
              {/*console.warn('User with this email does not exist');*/}
              ToastAndroid.showWithGravity(
                'O máximo de jogadores por partida é 7 ',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
              break;
            default:
              {/*console.warn('Check your internet connection');*/}
              ToastAndroid.showWithGravity(
                'O máximo de jogadores por partida é 7 ',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
          }
          resolve(false);
        });
    })
  };

}

export default new MyFirebase();